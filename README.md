
<!-- README.md is generated from README.Rmd. Please edit that file -->

Below is a look into racial bias in Seattle’s [Terry
stop](https://en.wikipedia.org/wiki/Terry_stop) data which I did as part
of volunteer work for [Data For
Democracy](http://datafordemocracy.org/). The analysis is based on a
paper which examines “Stop-and-Frisk” in New York City which can be
found
[here](http://www.stat.columbia.edu/~gelman/research/published/frisk9.pdf).

# Data

## Seattle Incident-Based Reporting

Below are maps which show the spatial locations of all Seattle Police
Department (SPD) incidents in 2016 for which data is available. The full
data set is available
[here](https://data.seattle.gov/Public-Safety/Seattle-Police-Department-Police-Report-Incident/7ais-f98f).
Each map corresponds to one of the Uniform Crime Reporting (UCR)’s 12
categories while the colored areas delineate one of SPD’s 17 police
sectors.

![](fig/sibr-1.png)<!-- -->

## National Incident-Based Reporting

Unfortunately, the data presented above do not include any demographic
information; this is obviously problematic when one is interested in
examining crime data along racial dimensions. Fortunately, SPD
participates in the National Incident-Based Reporting System
([NIBRS](https://www.icpsr.umich.edu/icpsrweb/ICPSR/series/128)) and
some of the records – approximately 40% – submitted into this system do
contain demographic information however these data do not include any
spatial information.

To utilize both sources, the SPD and NIBRS are matched by incident
datetime and type of crime which, of course, only works for instances in
which this information is unique (otherwise it is not possible to
match). Using this strategy, approximately 14% of the records can be
matched. The significance of this is that the matched data provide
demographic information by both sector AND race.

Aggregate counts by race/ethnicity are summarized below:

![](fig/mibr-1.png)<!-- -->

where: A indicates Asian, B indicates black, H indicates Hispanic, I
indicates Alaska Native or American Indian, and W indicates white. The
solid portion of each bar in the plot above represents data found in the
Arrestee segment of the NIBRS data set while the transparent portion
represents data found in the Offender segment of the data set.

## Terry stops

SPD’s Terry stop data can be found
[here](https://data.seattle.gov/Public-Safety/Terry-Stops/28ny-9ts8).
The aggregated demographic information – again, counts by race – is
summarized below and one can see that the pattern is quite similar to
that found in the incidents data:

![](fig/terry-1-1.png)<!-- -->

A, B, H, I, and W are defined as above and – in this case – the solid
portion of each bar represents stops that led to an arrest while the
transparent portion represents stops that did not.

## American Community Survey

Demographic data for each police sector are obtained by utilizing the
2015 five-year estimates of the American Community Survey (ACS). These
data are available at the block-group level of which the city contains
482; of these, 389 lie completely within one of the the 17 police
sectors. For those block-groups lying in multiple sectors, the
percentage of area of overlap between the sector and the block-group is
used to spatially allocate the data.

For example, approximately 30% of the area of Block Group 6 of Census
Tract 32 is located in sector B and so sector B is apportioned 30% of
that block-group’s population, etc.

A natural point of comparison is the demographic profile of the city as
a whole. This is summarized below:

![](fig/acs-race-1.png)<!-- -->

In comparison to the counts of arrests and stops, one can see that
blacks are dramatically over represented and it is exactly this type of
race-based disparity that the city is hoping to address. For example,
see [here](https://www.seattle.gov/rsji/about).

With respect to the rest of the analysis, the following demographic and
economic data from the ACS are also used:

<table>
<caption>
Table of means for ACS data
</caption>
<thead>
<tr>
<th style="text-align:left;">
</th>
<th style="text-align:center;">
Mean
</th>
<th style="text-align:center;">
SD
</th>
<th style="text-align:center;">
Min
</th>
<th style="text-align:center;">
Max
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;">
Median Income
</td>
<td style="text-align:center;">
61830.20
</td>
<td style="text-align:center;">
14767.72
</td>
<td style="text-align:center;">
33943.99
</td>
<td style="text-align:center;">
85110.19
</td>
</tr>
<tr>
<td style="text-align:left;">
Proportion black
</td>
<td style="text-align:center;">
0.09
</td>
<td style="text-align:center;">
0.07
</td>
<td style="text-align:center;">
0.01
</td>
<td style="text-align:center;">
0.24
</td>
</tr>
<tr>
<td style="text-align:left;">
Proportion with four year degree
</td>
<td style="text-align:center;">
0.62
</td>
<td style="text-align:center;">
0.15
</td>
<td style="text-align:center;">
0.32
</td>
<td style="text-align:center;">
0.79
</td>
</tr>
</tbody>
</table>

Ultimately, Hispanic data will not be used in the analysis This is for
two reasons. First, this information is not tracked uniformly; NIBRS and
ACS consider Hispanic to be an ethnicity distinct from race while the
data available from SPD do not. Second, there just isn’t that much
Hispanic data to consider.

# Models

## Approach 1

For each race $`r=\{A, B, I, W\}`$ and police sector $`s`$, the annual
number of Terry stops $`y_{rst}`$ is modeled by an overdispersed Poisson
regression using the number of crimes $`\theta_{rs}`$ as an offset:

``` math
y_{rst} \sim \text{Poisson} \left( \theta_{rs} e^{\mu + \alpha_r + \beta_s + \epsilon_{rst}} \right)
```

where the $`\alpha_r`$’s account for variation between races, the
$`\beta_s`$’s account for variation between sectors, and the
$`\epsilon_{rs}`$’s allow for overdispersion. The subscript $`t`$ is
used explicitly since two years of data – 2015 and 2016 – are available.

$`\theta_{rs}`$ – of course – is not observed. Thus, the number of
previous year’s arrests $`a_{rs(t-1)}`$ is used as a proxy:

``` math
y_{rst} \sim \text{Poisson} \left( a_{rs(t-1)} e^{\mu + \alpha_r + \beta_s + \epsilon_{rst}} \right)
```

In addition to the model above, two alternative specifications are
considered. First, the offset $`a_{rs(t-1)}`$ is modeled more flexibly
by including it as a predictor with its own coefficient:

``` math
y_{rst} \sim \text{Poisson} \left( e^{\gamma \log(a_{rs(t-1)}) + \mu + \alpha_r + \beta_s + \epsilon_{rst}} \right)
```

Second, the proportion of the sector’s population that is black
$`z_{1s}`$ is added as a predictor:

``` math
y_{rst} \sim \text{Poisson} \left( a_{rs(t-1)} e^{\mu + \alpha_r + \beta_s + \rho z_{1s} + \epsilon_{rst}} \right)
```

## Approach 2

As an alternative approach, the number of arrests $`a_{rst}`$ are
assumed to be a realization of the true crime rate:

``` math
\begin{split}
y_{rst} &\sim \text{Poisson} \left( \theta_{rs} e^{\mu + \alpha_r + \beta_s + \epsilon_{rst}} \right) \\
a_{rst} &\sim \text{Poisson} \left( \theta_{rs} \right)
\end{split}
```

with the rate then modeled as function of each race’s population in the
sector $`p_{rs}`$ and an additional set of race and sector parameters
$`\delta_r`$ and $`\zeta_s`$:

``` math
\log(\theta_{rs}) = \log(p_{rs}) + \nu + \delta_r + \zeta_s + \upsilon_{rs}
```

Two additional specifications of the model for $`\theta_{rs}`$ are also
considered. First, the population $`p_{rs}`$ is given its own
coefficient:

``` math
\log(\theta_{rs}) = \kappa \log(p_{rs}) + \nu + \delta_r + \zeta_s + \upsilon_{rs}
```

Second, each sector’s median income $`z_{2s}`$ and proportion of the
population with at least a four-year degree $`z_{3s}`$ are included as
predictors:

``` math
\log(\theta_{rs}) = \log(p_{rs}) + \nu + \delta_r + \zeta_s + \phi z_{2s} + \psi z_{3s} + \upsilon_{rs}
```

## Approach 3

As a final approach, a model is considered where overdispersion in the
model for $`a_{rst}`$ is allowed for:

``` math
\begin{split}
y_{rst} &\sim \text{Poisson} \left( \theta_{rs} e^{\mu + \alpha_r + \beta_s + \epsilon_{rst}} \right) \\
a_{rst} &\sim \text{Poisson} \left( \theta_{rs} e^{\omega_{rst}} \right)
\end{split}
```

Similar specifications for $`\theta_{rs}`$ as above are used in this
case with the exception of the $`\upsilon_{rs}`$ term.

## Inference

The models are fit using Stan with the following normal priors:

``` math
\begin{split}
\alpha_r &\sim N(0, \sigma_{\alpha}^2) \\
\beta_s &\sim N(0, \sigma_{\beta}^2) \\
\epsilon_{rs} &\sim N(0, \sigma_{\epsilon}^2)
\end{split}
```

where $`\sigma_{\alpha}`$, $`\sigma_{\beta}`$, and $`\sigma_{\epsilon}`$
represent the variation in the parameters for race, sector, and
overdispersion respectively. A similar set of priors are used for the
approaches which explicitly model $`\theta_{rs}`$:

``` math
\begin{split}
\delta_r &\sim N(0, \sigma_{\delta}^2) \\
\zeta_s &\sim N(0, \sigma_{\zeta}^2) \\
\upsilon_{rs} &\sim N(0, \sigma_{\upsilon}^2) \\
\omega_{rs} &\sim N(0, \sigma_{\omega}^2)
\end{split}
```

The above $`\sigma`$ hyperparameters – as well as all other parameters
in each of the models – are given flat priors.

<!-- ```math -->
<!-- \begin{split} -->
<!-- \sigma_{\alpha} &\sim N_{\geq 0}(0, 1) \\ -->
<!-- \sigma_{\delta} &\sim N_{\geq 0}(0, 1) -->
<!-- \end{split} -->
<!-- ``` -->
<!-- where $`N_{\geq 0}`$ denotes the half-normal distribution with non-negative support. -->

# Results

In the models for $`y_{rst}`$, the race and sector coefficients are
considered relative to their mean by making the following adjustments:

``` math
\begin{split}
\alpha_r^{adj} &= \alpha_r - \bar \alpha \\
\beta_s^{adj} &= \beta_s - \bar \beta \\
\mu^{adj} &= \mu + \bar \alpha + \bar \beta
\end{split}
```

Posterior estimates are presented in the table below:

|  | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| $\mu^{adj}$ | -0.66<br>(0.03) | 0.27<br>(0.24) | -0.66<br>(0.03) | -0.67<br>(0.03) | -0.67<br>(0.03) | -0.67<br>(0.03) | -0.68<br>(0.06) | -0.68<br>(0.05) | -0.68<br>(0.06) |
| $\gamma$ |  | 0.76<br>(0.06) |  |  |  |  |  |  |  |
| $\rho$ |  |  | 0.06<br>(0.12) |  |  |  |  |  |  |
| $\sigma_\beta$ | 0.19<br>(0.05) | 0.14<br>(0.05) | 0.20<br>(0.06) | 0.20<br>(0.05) | 0.20<br>(0.05) | 0.20<br>(0.05) | 0.09<br>(0.07) | 0.11<br>(0.07) | 0.10<br>(0.07) |
| $\sigma_\epsilon$ | 0.26<br>(0.04) | 0.28<br>(0.04) | 0.26<br>(0.04) | 0.20<br>(0.03) | 0.19<br>(0.03) | 0.19<br>(0.03) | 0.47<br>(0.04) | 0.36<br>(0.03) | 0.47<br>(0.04) |

Estimates and standard errors for the Poisson regression model of
$y_{rst}$

Excluding the model that includes $`\gamma`$, the estimates of
$`\mu^{adj}`$ indicate that prior to considering the race and sector
effects, on average, the stop rates are approximately $`e^{-0.66}`$ or
50% lower than the prior year’s arrest rates. The magnitude of this
coefficient is directly related to the missingness in the stops data –
recall, that only about 14% of the NIBRS data could be matched to the
SPD data. Including the proportion of the sector’s population that is
black has no discernible impact on the model as $`\rho`$ is
indistinguishable from zero.

Race and sector effects – expressed as $`e^{\alpha_r}`$ and
$`e^{\beta_s}`$ – are presented below:

![](fig/plots-one-1.png)<!-- -->

where the points indicate the posterior means and the error bars
indicate the 95% HPD intervals. The natural point of comparison here is
the value one which would indicate that relative stops – either among
the races or the sectors – were not higher or lower than the prior
year’s arrests would suggest.

From the graph one can see that Asians and blacks are stopped relatively
less, Alaska Natives and American Indians are stopped relatively more
(noticeably so!), while stops of whites are consistent with the average
pattern observed between stops and arrests (i.e. a 50% difference).

In the models for $`\theta_{rst}`$, the race and sector coefficients are
also considered relative to their mean by making the similar
adjustments:

``` math
\begin{split}
\delta_r^{adj} &= \delta_r - \bar \delta \\
\zeta_s^{adj} &= \zeta_s - \bar \zeta \\
\nu^{adj} &= \nu + \bar \delta + \bar \zeta
\end{split}
```

Posterior estimates are presented in the table below:

|  | 4 | 5 | 6 | 7 | 8 | 9 |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| $\nu^{adj}$ | -3.90<br>(0.06) | 0.66<br>(0.58) | -3.90<br>(0.06) | -3.92<br>(0.04) | 0.07<br>(0.31) | -3.92<br>(0.04) |
| $\kappa$ |  | 0.42<br>(0.07) |  |  | 0.49<br>(0.04) |  |
| $\phi$ |  |  | -1.07<br>(0.46) |  |  | -1.01<br>(0.50) |
| $\psi$ |  |  | 0.24<br>(0.45) |  |  | 0.19<br>(0.47) |
| $\sigma_\zeta$ | 0.87<br>(0.18) | 0.58<br>(0.13) | 0.75<br>(0.17) | 0.92<br>(0.19) | 0.62<br>(0.12) | 0.82<br>(0.17) |
| $\sigma_\upsilon$ | 0.45<br>(0.05) | 0.30<br>(0.04) | 0.46<br>(0.05) |  |  |  |
| $\sigma_\omega$ |  |  |  | 0.40<br>(0.03) | 0.26<br>(0.03) | 0.40<br>(0.03) |

Estimates and standard errors for the Poisson regression model of
$\theta_{rs}$

Excluding models that include $`\kappa`$, the estimates of $`\nu^{adj}`$
indicate that prior to considering the race and sector effects – on
average – the arrest rates correspond to approximately $`e^{-3.90}`$ or
2% of the sector’s population.

Higher median income is associated with a lower true crime rate; the
posterior mean for the coefficient suggests that there is a 33%
reduction in the crime rate when median income changes from
approximately 47,000 to 76,000 dollars. The proportion of the sector’s
population with a four-year degree is positively associated with the
true crime rate although this value is not distinguishable from zero.

Race and sector effects – again, expressed as $`e^{\delta_r}`$ and
$`e^{\zeta_s}`$ are presented below:

![](fig/plots-two-1.png)<!-- -->

Once again, the natural point of comparison here is the value one.
However, in this case a value of one indicates that the relative (and
inferred) crime rate in the sector is not higher or lower given the
sector’s population.

With respect to race, the graph is consistent with what the previous
plots illustrated: that blacks are significantly overrepresented in
crime. To a lesser extent this also true of Alaska Natives and American
Indians. Asians and whites are underrepresented.

With respect to sector, one can see that in some cases relative crimes
are also noticeably higher. In particular, this is true for sectors K,
M, and O which correspond to the downtown/south Seattle corridor. This
is not surprising given that these sectors have both high arrests rates
and low resident populations.

# Posterior Predictive Checks

Finally, one can compare the observed stops and arrests with what the
model would predict using simulation. The figure below shows the two
years of stops (points) versus the 95% HPD intervals for the predicted
stops (line segments). As is somewhat evident, the points often fall
within the range of the line segments.

![](fig/pp-stops-1.png)<!-- -->

Formalizing a bit more, we can check the percentage of times that both
$`y_{rst}`$ and $`a_{rst}`$ are consistent with the counts predicted by
the models. These classification rates are summarized in the table
below:

<table>
<caption>
Classification rates
</caption>
<thead>
<tr>
<th style="text-align:center;">
</th>
<th style="text-align:center;">
1
</th>
<th style="text-align:center;">
2
</th>
<th style="text-align:center;">
3
</th>
<th style="text-align:center;">
4
</th>
<th style="text-align:center;">
5
</th>
<th style="text-align:center;">
6
</th>
<th style="text-align:center;">
7
</th>
<th style="text-align:center;">
8
</th>
<th style="text-align:center;">
9
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:center;">
$y_{rst}$
</td>
<td style="text-align:center;">
33.8
</td>
<td style="text-align:center;">
41.9
</td>
<td style="text-align:center;">
46.3
</td>
<td style="text-align:center;">
5.1
</td>
<td style="text-align:center;">
5.9
</td>
<td style="text-align:center;">
3.7
</td>
<td style="text-align:center;">
49.3
</td>
<td style="text-align:center;">
48.5
</td>
<td style="text-align:center;">
36.8
</td>
</tr>
<tr>
<td style="text-align:center;">
$a_{rst}$
</td>
<td style="text-align:center;">
</td>
<td style="text-align:center;">
</td>
<td style="text-align:center;">
</td>
<td style="text-align:center;">
95.6
</td>
<td style="text-align:center;">
93.4
</td>
<td style="text-align:center;">
91.9
</td>
<td style="text-align:center;">
95.6
</td>
<td style="text-align:center;">
93.4
</td>
<td style="text-align:center;">
91.9
</td>
</tr>
</tbody>
</table>
