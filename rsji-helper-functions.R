
extract_mean <- function(m, labs = NULL) {

  fs <- summary(fit[[m]]$result)$summary

  pm <- str_extract_all(row.names(fs), '[^\\[\\d\\]]*')
  pm <- pm |> unlist() |> unique()
  pm <- pm[!pm %in% c('', 'lp', 'lp__')]

  out <- list()

  for (p in seq_along(pm)) {

    val <- fs[, 'mean'][str_detect(row.names(fs), pm[p])]

    race <- NA

    sect <- NA

    if (length(val) == 4) {

      race <- labs$r

    }

    if (length(val) == 17) {

      sect <- labs$s

    }

    if (length(val) == 68) {

      race <- rep(labs$r, 17)

      sect <- map(labs$s, rep, 4) |> unlist()

    }

    out[[p]] <- tibble(
      param = pm[p]
      , model = m
      , race = race
      , sector = sect
      , value = val
    )

  }

  do.call(rbind, out)

}

extract_pars <- function(m, pars = NULL, adjust = FALSE, labs = NULL) {

  x <- rstan::extract(fit[[m]]$result, pars = pars, inc_warmup = FALSE)[[1]] |>
    as_tibble()

  if (adjust & !pars %in% c('mu', 'nu')) {

    x <- x - rowSums(x) / ncol(x)

  }

  if (adjust & pars == 'mu') {

    y <- rstan::extract(fit[[m]]$result, pars = 'alpha', inc_warmup = FALSE)[[1]]
    y <- rowSums(y) / dim(y)[2]

    z <- rstan::extract(fit[[m]]$result, pars = 'beta', inc_warmup = FALSE)[[1]]
    z <- rowSums(z) / dim(z)[2]

    x <- x + y + z

  }

  if (adjust & pars == 'nu') {

    y <- rstan::extract(fit[[m]]$result, pars = 'delta', inc_warmup = FALSE)[[1]]
    y <- rowSums(y) / dim(y)[2]

    z <- rstan::extract(fit[[m]]$result, pars = 'zeta', inc_warmup = FALSE)[[1]]
    z <- rowSums(z) / dim(z)[2]

    x <- x + y + z

  }

  avg <- apply(x, 2, mean)

  std <- apply(x, 2, sd)

  hpds <- coda::HPDinterval(coda::mcmc(x))

  out  <- tibble(
    label = pars
    , model = m
    , mean  = avg
    , sd = std
    , lower = hpds[, 'lower']
    , upper = hpds[, 'upper']
  )

  if (!is.null(labs)) {

    out$label <- labs

  }

  if (is.null(labs) & nrow(out) > 1) {

    out$label <- str_c(pars, 1:nrow(out))

  }

  return(out)

}


extract_rhat <- function(m) {

  tibble(
    model = m
    , param = row.names(summary(fit[[m]]$result)$summary)
    , rhat  = summary(fit[[m]]$result)$summary[, 'Rhat']
  )

}


rpois_rnorm <- function(N, x, y) {

  replicate(N, rpois(1, exp(x + rnorm(1, 0, y))))

}


# Function for standardizing regression predictors by dividing by 2 sd'
arm_rescale <- function (x, binary.inputs="center") {

  # function to rescale by subtracting the mean and dividing by 2 sd's
  if (!is.numeric(x)){

    x <- as.numeric(factor(x))

    x.obs <- x[!is.na(x)]

  }

  x.obs <- x[!is.na(x)]

  # for binary cases
  if (length(unique(x.obs))==2) {

    if (binary.inputs=="0/1") {

      x <- (x-min(x.obs))/(max(x.obs)-min(x.obs))
      return (x)

    }
    else if (binary.inputs=="-0.5,0.5") {

      return(x - 0.5)

    }
    else if (binary.inputs == "center") {

      return(x - mean(x.obs))

    }
    else if (binary.inputs=="full") {

      return((x - mean(x.obs)) / (2 * sd(x.obs)))

    }
  }
  else {

    return((x - mean(x.obs)) / (2 * sd(x.obs)))

  }
  
}
